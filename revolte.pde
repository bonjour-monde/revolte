import ddf.minim.analysis.*;
import ddf.minim.*;

PImage[] images = new PImage[2];

Minim       minim;
AudioPlayer sound;
FFT         fft;
FFT         fftL;
FFT         fftR;
BeatDetect  beat;



//pixel object
// there some cool effect if you indicate the wrong image size
Pixel[] px = new Pixel[1754*1240];

//value for sound detection left
float f_L;
float diffL;

//value for sound detection right
float f_R;
float diffR;
//for line
float oldX, oldY;
//size of the pixel copy
float size = 1;

//number of illustrations
int page = 1;
//number image
int j = 0;
//image size
int w = 1754;
int h = 1240;


void setup() {
  size(1754, 1240);
  background(255);
  noLoop();
  for ( int i = 0; i < images.length; i++ ){
    images[i] = loadImage( "phototest-1.jpg" );
  }


  minim = new Minim(this);
  beat = new BeatDetect();
  // specify that we want the audio buffers of the AudioPlayer
  // to be 1024 samples long because our FFT needs to have
  // a power-of-two buffer size and this is a good size.
  sound = minim.loadFile("portail.wav", 1024);

  // loop the file indefinitely
//sound.loop();


  smooth();
  noStroke();
  
  fftL = new FFT( sound.bufferSize(), sound.sampleRate() );
  fftR = new FFT( sound.bufferSize(), sound.sampleRate() );

}

void draw() {

  //jouer chaque instant de musique à analyser
  float t = sound.length();
  float sample = t/50;
  //
  //charger une images diff suivant array above/
  //

    for (int i = 0; i < page; i ++){
    if(j < 12){
      // transform each pixel into an object;
      for (int x = 0; x < w; x++) {
        for (int y = 0; y < h; y++ ) {
            int loc = x + y*w;
            color pix = images[j].get(x, y);
            //println(pix);
            px[loc] = new Pixel(x, y, loc, pix);


        }
      }
      //reset background
      //fill(255);
      fill(255);
      rect(0,0,width,height);

      sound.play(int(i*sample));
      delay(int(sample));
      //stroke(255);

      //analyser, bouger pixel et exporter une image
        for (int x = 0; x < w; x++) {
          for (int y = 0; y < h; y++ ) {
              int loc = x + y*w;
              color pix = images[j].get(x, y);
              //println(pix);
              pushMatrix();
              translate(0,0);
              px[loc].move();
              px[loc].display();
              popMatrix();
          }
        }

      sound.rewind();
      //link all points

      save("exportHard5-01.png");
      j++;
    }else{
    j = 0;
  }
}
}

class Pixel {
	//Déclaration des paramètres de base de la balle
  float x, y, loc ;
  color couleur;

  //constructeur de l'objet (une sorte de setup() )
  Pixel (float nouvX, float nouvY, float location, color nouvCouleur) {
    x = nouvX;
    y = nouvY;
    loc  = location;
    couleur = nouvCouleur;
  }

  //déclaration des methodes
  void display() {
    // créer un rvb avec des modulo

    //fill(245,132,102);
      fill(couleur);
    //etllipse(x*size, y*size, size, size);
    // ellipse(x*size, y*size, size, size);


      //fill(0,132,0);
      //rect(x*size, y*size, size, size);
      noStroke();
      rect(x*size, y*size, size*2, size*2);

      // if(loc%2 == 0){
      //   noStroke();
      //   rect(x*size, y*size, size, size);
      // }
      // if(loc%3 == 0){
      //   noStroke();
      //   rect(x*size, y*size, size, size);
      // }
    }

  void move() {

    fftL.forward(sound.left);
    fftR.forward(sound.right);
    
    float r = 0.5;

    float n = map(loc, 0, w*h, 0, 15000);
    
    diffL = f_L - fftL.getFreq(n);
    f_L = fftL.getFreq(n); //get Freq Hz
    diffR = f_R - fftL.getFreq(n);
    f_R = fftR.getFreq(n); //get Freq Hz
    if(diffL < 0){
      y = y - f_R*r;
      x = x - f_L*r;
    }else{
      y = y + f_R*r;
      x = x + f_L*r;
    }



  }


}
